# CEP - GraphQL + Vue (Nuxt)

> **Typescript** Required - Testing: **v3.4.5**

Site **Front-end** para consumir da API https://viacep.com.br via Apollo Client.

![frontend](https://gitlab.com/i.leonardo/ativ-nuxt-cep/raw/master/frontend.png)

Servidor **Back-end** feito em **Apollo Server** para entregar as respostas via Apollo Client e ter o controle dos dados que serão consultados.

![backend](https://gitlab.com/i.leonardo/ativ-nuxt-cep/raw/master/backend.png)

## Instalação

Backend (Javascript ES6 + Babel)

``` bash
# install dependencies
$ cd backend
$ npm install

# Start Server Watch in http://localhost:4000 (default)
$ npm run serve

# Start Server production http://localhost:4000 (default)
$ npm start
```

Frontend (Typescript + Nuxt)

```bash
# install dependencies
$ cd frontend
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# Validate or fix code with the rules of the extends: plugin:vue/recommended, @vue/airbnb
$ npm run lint

# build for production and launch server
$ npm run build
$ npm start

# generate static project (root /dist)
$ npm run generate
```

## Árvore (tree)

```
.
|   .gitignore
|   LICENSE
|   README.md
|
+---backend
|   |   .babelrc
|   |   .editorconfig
|   |   .gitignore
|   |   package.json
|   |   README.md
|   |
|   \---src
|       |   app.js (init)
|       |   resolvers.js (import Queries | Mutations | Schemas)
|       \   schema.graphql
|
\---frontend
    |   .browserslistrc (versions browsers support)
    |   .editorconfig
    |   .eslintrc.js
    |   .gitignore
    |   nuxt.config.ts (Nuxt CLI)
    |   package.json
    |   README.md
    |   tsconfig.json (ts-node)
    |
    \---src
        |   shims-gql.d.ts (import file '*.gql')
        |
        +---assets (medias)
        +---components
        +---graphql
        +---layouts (default unique)
        +---middleware
        +---pages (route '/')
        +---plugins
        +---static (favicon.ico)
        \---store (vuex not used)
```

## Licença

> [MIT License](https://gitlab.com/i.leonardo/ativ-nuxt-cep/blob/master/README.md)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)

