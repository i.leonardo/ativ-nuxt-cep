# Apollo Server

> Servidor GraphQL no Node.JS HTTP Server, ou em ambientes "sem servidor"

## Project Setp

```bash
# install dependencies
$ npm install

# Start Server Watch in http://localhost:4000 (default)
$ npm run serve

# Start Server production http://localhost:4000 (default)
$ npm start
```

Para mais informações [clique aqui](https://www.apollographql.com/docs/apollo-server/) para configurar o Apollo Server

