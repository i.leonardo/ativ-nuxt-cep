import axios from 'axios';
import { ApolloError, ValidationError } from 'apollo-server';

async function cep(cep) {
  try {
    const test = cep.length;
    if (test <= 7 || test > 8) throw new ValidationError('Cep Inválido.');

    const { data } = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);

    if (data.hasOwnProperty('erro')) throw new ApolloError('Cep não localizado.');
    return data;
  } catch(e) {
    throw new ApolloError(e);
  }
}

export default cep;
