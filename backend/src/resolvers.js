import Cep from './Queries/Cep';

export default {
  Query: {
    cep: (_, { cep }) => Cep(cep)
  },
};
